# Get started
```bash
git clone git@gitlab.com:vovka667/terraform-provider-slack.git
dep ensure
go build -o terraform-provider-slack
terraform init
terraform plan
```
