provider "slack" {
	token = "${var.slack_token}"
}

resource "slack_user" "paolo" {
	first_name = "Paolo"
	last_name = "Sechi"
	email = "vladimir@gesundheitscloud.de"
}
