package main

import (
	"errors"

	"github.com/hashicorp/terraform/helper/schema"
)

func resourceUser() *schema.Resource {
	return &schema.Resource{
		Create: resourceUserCreate,
		Read:   resourceUserRead,
		Update: resourceUserUpdate,
		Delete: resourceUserDelete,

		Schema: map[string]*schema.Schema{
			"first_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"last_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func resourceUserCreate(d *schema.ResourceData, m interface{}) error {
	firstName := d.Get("first_name").(string)
	lastName := d.Get("last_name").(string)
	email := d.Get("email").(string)

	api := m.(*Config).API
	teamInfo, err := api.GetTeamInfo()
	if err != nil {
		return err
	}

	api.InviteToTeam(teamInfo.Name, firstName, lastName, email)
	d.SetId(email)
	return nil
}

func resourceUserRead(d *schema.ResourceData, m interface{}) error {
	api := m.(*Config).API
	email := d.Get("email").(string)
	user, err := api.GetUserByEmail(email)
	if err != nil {
		return err
	}

	profile, err := api.GetUserProfile(user.ID, false)
	if err != nil {
		return err
	}

	//TODO: call d.SetId("") if user doesn't exist

	d.Set("first_name", profile.FirstName)
	d.Set("last_name", profile.LastName)
	d.Set("email", profile.Email)

	return nil
}

func resourceUserUpdate(d *schema.ResourceData, m interface{}) error {
	return errors.New("Not implemented")
}

func resourceUserDelete(d *schema.ResourceData, m interface{}) error {
	api := m.(*Config).API
	teamInfo, err := api.GetTeamInfo()
	if err != nil {
		return err
	}

	email := d.Get("email").(string)
	user, err := api.GetUserByEmail(email)
	if err != nil {
		return err
	}

	err = api.DisableUser(teamInfo.Name, user.ID)
	if err != nil {
		return err
	}

	return nil
}
