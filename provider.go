package main

import (
	"github.com/hashicorp/terraform/helper/schema"
	"github.com/nlopes/slack"
)

type Config struct {
	API *slack.Client
}

func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"token": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("SLACK_API_KEY", nil),
				Description: "The api key to use for interacting with your Slack team.",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"slack_user": resourceUser(),
		},
		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	token := d.Get("token").(string)
	api := slack.New(token)
	api.SetDebug(true)

	config := &Config{
		API: api,
	}

	return config, nil
}
