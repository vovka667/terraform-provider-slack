variable "slack_token" {
  description = "The client secret to use. Remember, this should not be commited to SCM and passed in using -var-file=<secret.tfvars>"
}
